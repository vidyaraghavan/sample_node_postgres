Node.js Postgres Sample 
===============================

An example using Postgres on Shippable with the Knex module.

To run the tests:

`npm test`

Or, preferably, deploy on Shippable.com.
[![Build Status](https://apibeta.shippable.com/projects/541fde6a76d0c288e441e542/badge?branchName=master)](https://appbeta.shippable.com/projects/541fde6a76d0c288e441e542/builds/latest)